from authentication import (
    AuthenticationBackend, AuthenticationError, SimpleUser,
    UnauthenticatedUser, AuthCredentials, requires
)
from starlette.templating import Jinja2Templates
from starlette.responses import RedirectResponse, JSONResponse
from starlette.routing import Route

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database
from orm.general import Base, Users, Visitors, Checkpoints, Checkout
from passlib.hash import pbkdf2_sha256
import settings
import datetime as dt
import time

import io
from PIL import Image
import base64
import cv2

from modules.ocr import DrawOCR, numericalDetectron2, boundingBoxesDetectron2, alphabeticalDetectron2, easypredict, cfg
from modules.face_det import FaceDetection
drawer_ocr = DrawOCR(cfg['drawOCR'])
bBoxDet = boundingBoxesDetectron2(cfg['boundingBoxesDetectron2'])
numDet = numericalDetectron2(cfg['numericalDetectron2'])
alphaDet = alphabeticalDetectron2(cfg['alphabeticalDetectron2'])

def decode_img(img):
    img_data = str.encode(img)
    img = img_data[img_data.find(b'/9'):]
    ig = io.BytesIO(base64.b64decode(img))
    return ig

class ExtendedUser(SimpleUser):
    def __init__(self, username: str, is_admin: False) -> None:
        self.username = username
        self.admin_status = is_admin

    @property
    def is_admin(self) -> bool:
        return self.admin_status


class SessionAuthBackend(AuthenticationBackend):
    async def authenticate(self, request):

        username = request.session.get("user")
        if username is None:
            return

        credentials = ["authenticated"]
        is_admin = False

        if request.session.get("is_admin", False):
            credentials.append("admin")
            is_admin = True

        return AuthCredentials(credentials), ExtendedUser(username, is_admin)

class BasicAuth:
    def __init__(self, parent_templates):
        # DATABASE HANDLER
        # init database
        self.engine = create_engine(str(settings.DATABASE_URL))

        # create database if not exist
        if not database_exists(self.engine.url):
            create_database(self.engine.url)

        # create table if not exists
        Base.metadata.create_all(self.engine)
        self.session = sessionmaker()
        self.session.configure(bind=self.engine)
        # END DATABASE HANDLER

        self.__templates = parent_templates
        self.routes = []
        self.routes.append(Route('/login', endpoint=self.login, name='login', methods=["GET", "POST"]))
        self.routes.append(Route('/logout', endpoint=self.logout, name='logout', methods=["GET"]))
        self.routes.append(Route('/user_management', endpoint=self.user_management, name='user_management', methods=["GET", "POST"]))
        self.routes.append(Route('/registration', endpoint=self.registration, name='registration', methods=["GET", "POST"]))
        self.routes.append(Route('/scanner', endpoint=self.scanner, name='scanner', methods=["GET", "POST"]))
        self.routes.append(Route('/timeline_check', endpoint=self.timeline_check, name='timeline_check', methods=["GET", "POST"]))
        self.routes.append(Route('/timeline_checkout', endpoint=self.timeline_checkout, name='timeline_checkout', methods=["GET", "POST"]))
        self.routes.append(Route('/checkinout', endpoint=self.checkinout, name='checkinout', methods=["GET", "POST"]))
        self.routes.append(Route('/checkout', endpoint=self.checkout, name='checkout', methods=["GET", "POST"]))
        self.routes.append(Route('/api/scan_id', endpoint=self.scan_id, name='api_scan_id', methods=["POST"]))
        self.routes.append(Route('/api/update_user', endpoint=self.update_user, name='api_update_user', methods=["POST"]))
        self.routes.append(Route('/api/update_visitor', endpoint=self.update_visitor, name='api_update_visitor', methods=["POST"]))
        self.routes.append(Route('/api/update_checkin', endpoint=self.update_checkin, name='api_update_checkin', methods=["POST"]))
        self.routes.append(Route('/api/update_checkout', endpoint=self.update_checkout, name='api_update_checkout', methods=["POST"]))
        self.routes.append(Route('/api/time_checkout', endpoint=self.time_checkout, name='api_time_checkout', methods=["GET", "POST"]))


    async def login(self, request):
        context = {"request": request}
        context['messages'] = None
        if request.method == 'GET':
            """Shows the login page."""
            if request.session.get("user") is None:
                context['url_login_post'] = request.url_for('login')
                if (request.query_params.get('next') is not None) and (request.query_params.get('next') != ''):
                    context['url_login_post'] = request.url_for('login') + '?next=' + request.query_params.get('next')
                return self.__templates.TemplateResponse('/auth/login.html', context)
            else:
                if (request.query_params.get('next') is not None) and (request.query_params.get('next') != ''):
                    return RedirectResponse(url=request.query_params.get('next'))
                return RedirectResponse(url='/')
        elif request.method == 'POST':
            """Evaluate the submitted login information. Redirects to index page if login information valid, otherwise back to login."""
            sess = self.session()
            form = await request.form()
            if (form['username'] == 'admin') and (form['password'] == 'adminADI123'):
                request.session.update({'user': 'super_admin'})
                request.session.update({"is_admin": True})
                if (request.query_params.get('next') is not None) and (request.query_params.get('next') != ''):
                    return RedirectResponse(url=request.query_params.get('next'), status_code=303)
                return RedirectResponse(url='/', status_code=303)
            else:
                user = sess.query(Users).filter(Users.username == form['username']).filter(Users.deleted_at.is_(None)).first()
                if user is not None:
                    if pbkdf2_sha256.verify(form['password'], user.password):
                        user.is_login = 1
                        user.last_login = dt.datetime.now()
                        sess.add(user)
                        sess.commit()
                        request.session.update({'user': form['username']})
                        if user.is_admin:
                            request.session.update({"is_admin": True})
                        if (request.query_params.get('next') is not None) and (request.query_params.get('next') != ''):
                            return RedirectResponse(url=request.query_params.get('next'), status_code=303)
                        return RedirectResponse(url='/', status_code=303)
                sess.close()
            context['messages'] = 'Your username or password is wrong, please try again!'
            return self.__templates.TemplateResponse('/auth/login.html', context)

    @requires(['authenticated'], redirect='login')
    async def logout(self, request):
        """Logouts the users by clearing the session cookie."""
        context = {"request": request}
        user = request.session['user']
        request.session.clear()
        if user != 'super_admin':
            sess = self.session()
            user = sess.query(Users).filter(Users.username == user).first()
            user.is_login = 0
            user.last_logout = dt.datetime.now()
            sess.add(user)
            sess.commit()
            sess.close()
        return self.__templates.TemplateResponse('/auth/logout.html', context)

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def user_management(self, request):
        """
        Return an HTML index page.
        """
        sess = self.session()
        data = {'request': request}
        if request.method == 'POST':
            body = await request.form()
            if body.get('id', None) is not None:
                user = sess.query(Users).filter(Users.id == int(body['id'])).first()
                user.password = pbkdf2_sha256.hash(body.get('new_password'))
            else:
                user = Users(
                    username=body.get('username'),
                    password=pbkdf2_sha256.hash(body.get('password')),
                    is_admin=1 if body.get('is_admin') is not None else 0
                )
            sess.add(user)
            sess.commit()
        data['users'] = sess.query(Users).filter(Users.deleted_at.is_(None)).all()
        sess.close()
        return self.__templates.TemplateResponse('/auth/user_management.html', context=data)

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def registration(self, request):
        """
        Return an HTML index page.
        """
        sess = self.session()
        data = {'request': request}
        if request.method == 'POST':
            body = await request.form()
            # print(body)
            if body.get('id', None) is not None:
                visitor = sess.query(Visitors).filter(Visitors.id == int(body['id'])).first()
            else:
                img_data = body.get('result_face')
                # print(img_data)
                ig = decode_img(img_data)

                d = dt.datetime.now()
                unixtime = time.mktime(d.timetuple())
                face = './face_id/'+str(unixtime)[:-2]+'.jpg'

                im = Image.open(ig).save(face)
                
                FaceFileName, status = FaceDetection.detected(face)
                
                visitor = Visitors(
                    vis_name=body.get('name_visitor'),
                    identity_number=body.get('identity_number'),
                    identity_type=body.get('identity_type'),
                    gender=body.get('gender'),
                    phone=body.get('phone'),
                    office=body.get('office'),
                    address=body.get('address'),

                    face_status=status,
                    face_img=face,

                    added_by=request.session['user']
                )
            sess.add(visitor)
            sess.commit()
        sess.close()
        return self.__templates.TemplateResponse('/auth/registration.html', context=data)

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def scan_id(self, request):
        """
        Return an HTML index page.
        """
        if request.method == 'POST':
            body = await request.json()
            data = {}
            # print('scan')
            # print(body)
            img=body.get('identity_path')
            ig = decode_img(img)

            d = dt.datetime.now()
            unixtime = time.mktime(d.timetuple())
            iden = './card_id/'+str(unixtime)[:-2]+'.jpg'

            im = Image.open(ig).save(iden)
            
            image = cv2.imread(iden)
            c = body.get('identity_type')
            # print('type '+str(c))
            if str(c)=='eKTP':
                data = easypredict(image, bBoxDet, numDet, alphaDet, input_type='ktp')
                data['input_type']='eKTP'
            elif str(c)=='SIM':
                data = easypredict(image, bBoxDet, numDet, alphaDet, input_type='sim')
                data['input_type']='SIM'
            print(data)
        return JSONResponse(data)

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def scanner(self, request):
        """
        Return an HTML index page.
        """
        sess = self.session()
        data = {'request': request}
        data['visitors'] = sess.query(Visitors).filter(Visitors.deleted_at.is_(None)).all()
        sess.close()
        return self.__templates.TemplateResponse('/auth/scanner.html', context=data)

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def timeline_check(self, request):
        """
        Return an HTML index page.
        """
        sess = self.session()
        data = {'request': request}
        data['checkpoints'] = sess.query(Checkpoints).filter(Checkpoints.deleted_at.is_(None)).all()
        sess.close()
        return self.__templates.TemplateResponse('/auth/checkpoint.html', context=data)

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def timeline_checkout(self, request):
        """
        Return an HTML index page.
        """
        sess = self.session()
        data = {'request': request}
        data['checkpoints_out'] = sess.query(Checkout).filter(Checkout.deleted_at.is_(None)).all()
        sess.close()
        return self.__templates.TemplateResponse('/auth/checkpoint_out.html', context=data)

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def checkinout(self, request):
        """
        Return an HTML index page.
        """
        sess = self.session()
        data = {'request': request}
        if request.method == 'POST':
            body = await request.form()
            # print(body)
            if body.get('id', None) is not None:
                checkpoint = sess.query(Checkpoints).filter(Checkpoints.id == int(body['id'])).first()
            else:
                checkpoint = Checkpoints(
                    identity_number=body.get('identity_number'),
                    identity_type=body.get('identity_type'),
                    vis_name=body.get('vis_name'),
                    floor=body.get('floor'),
                    reason=body.get('reason'),

                    added_by=request.session['user']
                )
            sess.add(checkpoint)
            sess.commit()
        sess.close()
        return self.__templates.TemplateResponse('/auth/registration.html', context=data)

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def checkout(self, request):
        """
        Return an HTML index page.
        """
        sess = self.session()
        data = {'request': request}
        if request.method == 'POST':
            body = await request.form()
            # print(body)
            if body.get('id', None) is not None:
                checkkeluar = sess.query(Checkout).filter(Checkout.id == int(body['id'])).first()
            else:
                checkkeluar = Checkout(
                    identity_number=body.get('identity_number'),
                    identity_type=body.get('identity_type'),
                    vis_name=body.get('vis_name'),

                    added_by=request.session['user']
                )
            sess.add(checkkeluar)
            sess.commit()
        sess.close()
        return self.__templates.TemplateResponse('/auth/registration.html', context=data)

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def update_user(self, request):
        sess = self.session()
        data = {'request': request}
        if request.method == 'POST':
            body = await request.form()
            user = sess.query(Users).filter(Users.id == int(body['id'])).first()
            if body.get('action') is not None:
                if 'delete' in body.get('action'):
                    # soft delete item
                    user.deleted_at = dt.datetime.now()
                    user.username = 'deleted : '+user.username
                elif 'restore' in body.get('action'):
                    # restore item
                    user.deleted_at = None
                    user.username = user.username[10:]
                elif 'edit' in body.get('action'):
                    # edit item
                    temp = dict(body)
                    user.update(temp)
            else:
                # update is_predicted or is_tiles attribute
                user.update(body)
            sess.add(user)
            sess.commit()
            sess.close()
            return JSONResponse({'code': 200, 'messages': 'user has been modified!'})
    
    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def update_visitor(self, request):
        sess = self.session()
        data = {'request': request}
        if request.method == 'POST':
            body = await request.form()
            visitor = sess.query(Visitors).filter(Visitors.id == int(body['id'])).first()
            if body.get('action') is not None:
                if 'delete' in body.get('action'):
                    # soft delete item
                    visitor.deleted_at = dt.datetime.now()
                elif 'restore' in body.get('action'):
                    # restore item
                    visitor.deleted_at = None
                elif 'edit' in body.get('action'):
                    # edit item
                    temp = dict(body)
                    visitor.update(temp)
            else:
                # update is_predicted or is_tiles attribute
                visitor.update(body)
            sess.add(visitor)
            sess.commit()
            sess.close()
            return JSONResponse({'code': 200, 'messages': 'visitor has been modified!'})

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def update_checkin(self, request):
        sess = self.session()
        data = {'request': request}
        if request.method == 'POST':
            body = await request.form()
            checkpoint = sess.query(Checkpoints).filter(Checkpoints.id == int(body['id'])).first()
            if body.get('action') is not None:
                if 'delete' in body.get('action'):
                    # soft delete item
                    checkpoint.deleted_at = dt.datetime.now()
                elif 'restore' in body.get('action'):
                    # restore item
                    checkpoint.deleted_at = None
                elif 'edit' in body.get('action'):
                    # edit item
                    temp = dict(body)
                    checkpoint.update(temp)
            else:
                # update is_predicted or is_tiles attribute
                checkpoint.update(body)
            sess.add(checkpoint)
            sess.commit()
            sess.close()
            return JSONResponse({'code': 200, 'messages': 'visitor has been modified!'})

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def time_checkout(self, request):
        sess = self.session()
        data = {'request': request}
        if request.method == 'POST':
            body = await request.form()
            checkpoint = sess.query(Checkpoints).filter(Checkpoints.id == int(body['id'])).first()
            if body.get('action') is not None:
                if 'checkout' in body.get('action'):
                    # soft delete item
                    checkpoint.check_out = dt.datetime.now()
            else:
                # update is_predicted or is_tiles attribute
                checkpoint.update(body)
            sess.add(checkpoint)
            sess.commit()
            sess.close()
            return JSONResponse({'code': 200, 'messages': 'checkpoints has been modified!'})

    @requires(['authenticated'], redirect='login')
    @requires(['admin'], status_code=404)
    async def update_checkout(self, request):
        sess = self.session()
        data = {'request': request}
        if request.method == 'POST':
            body = await request.form()
            checkpoint = sess.query(Checkout).filter(Checkout.id == int(body['id'])).first()
            if body.get('action') is not None:
                if 'delete' in body.get('action'):
                    # soft delete item
                    checkpoint.deleted_at = dt.datetime.now()
                elif 'restore' in body.get('action'):
                    # restore item
                    checkpoint.deleted_at = None
                elif 'edit' in body.get('action'):
                    # edit item
                    temp = dict(body)
                    checkpoint.update(temp)
            else:
                # update is_predicted or is_tiles attribute
                checkpoint.update(body)
            sess.add(checkpoint)
            sess.commit()
            sess.close()
            return JSONResponse({'code': 200, 'messages': 'visitor has been modified!'})
