import os
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse
from starlette.templating import Jinja2Templates
from starlette.staticfiles import StaticFiles
from starlette.routing import Route, Mount
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.authentication import requires
from dotenv import dotenv_values
# from routers.crowd_counter import CrowdCounter
# from routers.vms import VMS
# from routers.parking_slot import ParkingSlot
# from routers.ocr import OCR
from routers.auth import ExtendedUser, SessionAuthBackend, BasicAuth
import settings

from uvicorn.config import Config
from uvicorn.supervisors import Multiprocess, StatReload
from uvicorn import Server

import signal
import sys
import logging

# INIT MODULES
modules = dotenv_values(os.path.join('/home/alam/Documents/smart_building_web_streaming-master/modules.ini'))

routes = []

path = os.path.abspath('.')
options = {
    "frame_size_reduction": 100,
    "frame_jpeg_quality": 80,
    "frame_jpeg_optimize": True,
    "frame_jpeg_progressive": False,
    "custom_data_location": path,
}

def modules_override(value, key):
    return modules.get(key, value)

# APPLICATION
# Application routes and function
templates = Jinja2Templates(directory='{}/templates'.format(path))
# add filter
templates.env.filters['modules_web'] = modules_override

# MODULES
# adding AI modules
# if int(modules['crowd']):
#     cc = CrowdCounter(logging=True, parent_templates=templates, **options)
#     routes = routes + [
#         Mount('/crowd', routes=cc.routes),
#     ]
# if int(modules['parking']):
#     ps = ParkingSlot(logging=True, parent_templates=templates, **options)
#     routes = routes + [
#         Mount('/parking', routes=ps.routes),
#     ]
# if int(modules['ocr']):
#     ocr = OCR(logging=True, parent_templates=templates, **options)
#     routes = routes + [
#         Mount('/ocr', routes=ocr.routes),
#     ]

# adding auth modules
auth_module = BasicAuth(parent_templates=templates)
routes = routes + [Mount('/auth', routes=auth_module.routes)]

@requires(['authenticated'], redirect='login')
def homepage(request):
    return templates.TemplateResponse('home.html', {'request': request})

def not_found(request, exc):
    """
    Return an HTML 404 page.
    """
    return templates.TemplateResponse('404.html', {'request': request}, status_code=exc.status_code)

def server_error(request, exc):
    """
    Return an HTML 500 page.
    """
    return templates.TemplateResponse('500.html', {'request': request}, status_code=exc.status_code)

# def startup():
#     print('Ready to go')

# def shutdown():
#     if int(modules['crowd']):
#         cc.shutdown()
#     if int(modules['parking']):
#         ps.shutdown()
#     print('Server disconnected')

# adding default routes
routes = routes + [
    Route('/', homepage, name='home'),
    Mount('/static', app=StaticFiles(directory='{}/static'.format(path)), name="static")
]

# exceptions handler
exception_handlers = {404: not_found, 500: server_error}

app = Starlette(debug=settings.DEBUG, routes=routes, exception_handlers=exception_handlers)
app.add_middleware(AuthenticationMiddleware, backend=SessionAuthBackend())
app.add_middleware(SessionMiddleware, secret_key=settings.SECRET_KEY, session_cookie="smart_building_session")

# UVICORN CUSTOM CLASS
class CustomServer(Server):
    def handle_exit(self, sig, frame):
        if self.should_exit:
            self.force_exit = True
        else:
            self.should_exit = True

def run(app, **kwargs):
    config = Config(app, **kwargs)
    server = CustomServer(config=config)

    if (config.reload or config.workers > 1) and not isinstance(app, str):
        logger = logging.getLogger("uvicorn.error")
        logger.warn(
            "You must pass the application as an import string to enable 'reload' or 'workers'."
        )
        sys.exit(1)

    if config.should_reload:
        sock = config.bind_socket()
        supervisor = StatReload(config, target=server.run, sockets=[sock])
        supervisor.run()
    elif config.workers > 1:
        sock = config.bind_socket()
        supervisor = Multiprocess(config, target=server.run, sockets=[sock])
        supervisor.run()
    else:
        server.run()

if __name__ == "__main__":
    run(app, host=settings.HOST, port=settings.PORT, http='h11', loop='asyncio')
