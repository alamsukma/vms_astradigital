# import the necessary packages
from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
from matplotlib import pyplot as plt
from PIL import Image
from skimage import io
import os

import datetime as dt
import time

class FaceDetection:
    def detected(image):
        detector = dlib.get_frontal_face_detector()
        image_pth = str(image)
        image = cv2.imread(image)
        # image = cv2.imwrite(image)
        image = imutils.resize(image, width=500)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        rects = detector(gray, 1)
        for (i, rect) in enumerate(rects):

            (x, y, w, h) = face_utils.rect_to_bb(rect)
            # cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
            # cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10),
            #             cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

            # Save just the rectangle faces in SubRecFaces
            sub_face = image[y-25:y+25+h, x-25:x+w+25]

            # d = dt.datetime.now()
            # unixtime = time.mktime(d.timetuple())

            face_crop = image_pth[:-4]+'_crop.jpg'
            cv2.imwrite(face_crop, sub_face)
            status = "Face Detected!"
            
        if not rects:
            face_crop = "No Face Detected!"
            status = "No Face Detected!"
        
        return face_crop, status


class LatestImage:
    def latest(dir_path, valid_extensions=('jpg', 'jpeg', 'png')):
        """
        Ambil gambar terakhir dari direktori
        """

        files = [os.path.join(dir_path, filename)
                 for filename in os.listdir(dir_path)]
        files = [f for f in files if '.' in f and
                 f.rsplit('.', 1)[-1] in valid_extensions and os.path.isfile(f)]

        if not files:
            raise ValueError("No files met requisites in %s" % dir_path)

        return max(files, key=os.path.getmtime)
