import databases
from starlette.config import Config
from starlette.datastructures import CommaSeparatedStrings, Secret

# INIT CONFIG
config = Config('config.ini')

DEBUG = config('DEBUG', cast=bool, default=False)
DATABASE_URL = config('DATABASE_URL', cast=databases.DatabaseURL)
SECRET_KEY = config('SECRET_KEY', cast=Secret)
PORT = config('PORT', cast=int, default=3000)
HOST = config('HOST', default='localhost')
# ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=CommaSeparatedStrings)
