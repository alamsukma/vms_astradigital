from sqlalchemy import Column, DateTime, String, Integer, func, Text, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from passlib.hash import pbkdf2_sha256
from sqlalchemy.types import LargeBinary

Base = declarative_base()

class Users(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True)
    password = Column(Text)
    is_admin = Column(Integer)
    is_login = Column(Integer)
    last_login = Column(DateTime)
    last_logout = Column(DateTime)
    created_at = Column(DateTime, default=func.current_timestamp())
    updated_at = Column(DateTime, default=func.current_timestamp(),
                        onupdate=func.current_timestamp())
    deleted_at = Column(DateTime)
    use = relationship("Visitors", back_populates="vis")
    use2 = relationship("Checkpoints", back_populates="check")
    use3 = relationship("Checkout", back_populates="check2")

    def update(self, newdata):
        for key, value in newdata.items():
            if (hasattr(self, key) or key != 'id'):
                if key == 'password':
                    value = pbkdf2_sha256.hash(value)
                setattr(self, key, value)

class Visitors(Base):
    __tablename__ = 'visitors'
    id = Column(Integer, primary_key=True)
    vis_name = Column(Text)
    # identity_img = Column(Text)
    identity_number = Column(String(16))
    identity_type = Column(Text)
    gender = Column(Text)
    phone = Column(String(12))
    office = Column(Text)
    address = Column(Text)

    face_img = Column(Text)
    face_status = Column(Text)
    identity_img = Column(Text)

    added_by = Column(String, ForeignKey('users.username'))

    created_at = Column(DateTime, default=func.current_timestamp())
    updated_at = Column(DateTime, default=func.current_timestamp(),
                        onupdate=func.current_timestamp())
    deleted_at = Column(DateTime)
    vis = relationship("Users", back_populates="use", uselist=False)

    def update(self, newdata):
        for key, value in newdata.items():
            if (hasattr(self, key) or key != 'id'):
                setattr(self, key, value)

class Checkpoints(Base):
    __tablename__ = 'checkpoints'
    id = Column(Integer, primary_key=True)
    identity_number = Column(String(16))
    identity_type = Column(Text)
    vis_name = Column(Text)
    floor = Column(Text)
    reason = Column(Text)

    added_by = Column(String, ForeignKey('users.username'))

    check_in = Column(DateTime, default=func.current_timestamp())
    check_out = Column(DateTime, default=func.current_timestamp(),
                        onupdate=func.current_timestamp())
    deleted_at = Column(DateTime)
    check = relationship("Users", back_populates="use2", uselist=False)

    def update(self, newdata):
        for key, value in newdata.items():
            if (hasattr(self, key) or key != 'id'):
                setattr(self, key, value)

class Checkout(Base):
    __tablename__ = 'checkpoints_out'
    id = Column(Integer, primary_key=True)
    identity_number = Column(String(16))
    identity_type = Column(Text)
    vis_name = Column(Text)

    added_by = Column(String, ForeignKey('users.username'))

    check_out = Column(DateTime, default=func.current_timestamp(),
                        onupdate=func.current_timestamp())
    deleted_at = Column(DateTime)
    check2 = relationship("Users", back_populates="use3", uselist=False)

    def update(self, newdata):
        for key, value in newdata.items():
            if (hasattr(self, key) or key != 'id'):
                setattr(self, key, value)